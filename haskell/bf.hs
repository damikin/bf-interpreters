import Data.Char

data Symbol =
      None
    | Inc
    | Dec
    | Forward
    | Backward
    | Output
    | Input
    | Loop [Symbol]
    deriving (Show)
type Program = [Symbol]

printProgram :: Program -> String
printProgram [] = ""
printProgram (Loop x:xs) =
    "[" ++ printProgram x ++ "] " ++ printProgram xs
printProgram (x:xs) = show x ++ " " ++ printProgram xs

parseString :: String -> Program
parseString [] = []
parseString s = reverse . fst $ parse [] s 
    where
        parse xs [] = (xs, [])
        parse xs ('+':ys) = parse (Inc:xs) ys
        parse xs ('-':ys) = parse (Dec:xs) ys
        parse xs ('>':ys) = parse (Forward:xs) ys
        parse xs ('<':ys) = parse (Backward:xs) ys
        parse xs ('.':ys) = parse (Output:xs) ys
        parse xs (',':ys) = parse (Input:xs) ys
        parse xs (']':ys) = (xs, ys)
        parse xs ('[':ys) =
            let
                (xs', ys') = parse [] ys
            in
                parse (Loop (reverse xs'):xs) ys'
        parse xs (_:ys) = parse xs ys

runProgram :: [Int] -> Program -> ([Int], [Int])
runProgram input prog =
    let
        start = (prog, input, [], repeat 0, 0, 0)
        (mem, out, _, maxInd) = run start
    in
        (take (maxInd+1) mem, reverse out)

    where
        updateMem (m:mem) 0 val = (m+val):mem
        updateMem (m:mem) ind val = m:(updateMem mem (ind-1) val)

        replaceMem (_:mem) 0 val = val:mem
        replaceMem (m:mem) ind val = m:(replaceMem mem (ind-1) val)

        getMem (m:_) 0 = m
        getMem (m:mem) ind = getMem mem (ind-1)

        run ([], _, out, mem, ind, imax) = (mem, out, ind, imax)
        run ((op:prog), input, out, mem, ind, imax) =
            case op of
                Inc ->
                    run (prog, input, out, updateMem mem ind 1, ind, imax)
                Dec ->
                    run (prog, input, out, updateMem mem ind (-1), ind, imax)
                Forward ->
                    run (prog, input, out, mem, ind+1, max imax $ ind+1)
                Backward ->
                    run (prog, input, out, mem, ind-1, imax)
                Output ->
                    run (prog, input, (getMem mem ind):out, mem, ind, imax)
                Input -> let (i:is) = input in
                    run (prog, is, out, replaceMem mem ind i, ind, imax)
                otherwise -> let Loop ls = op in
                    if (0==) $ getMem mem ind
                    then
                        run (prog, input, out, mem, ind, imax)
                    else
                        let {(mem', out', ind', imax') =
                            run (ls, input, out, mem, ind, imax)}
                        in run ((op:prog), input, out', mem', ind', imax')

main = do
    contents <- getContents
    let (_, out) = runProgram [] $ parseString contents
    putStr $ map chr out

