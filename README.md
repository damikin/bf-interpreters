# Info #

BF (https://en.wikipedia.org/wiki/Brainfuck) interpreters.  These are written on FreeBSD, some changes might be necessary for Linux.

### Languages ###

* SMLNJ
* Haskell
* C

### FreeBSD Installation ###

* pkg install smlnj
* pkg install ghc
* pkg install clang gmake

### Usage ###

SMLNJ:

* $ cd sml
* $ sml
* \> use "bf.sml";
* \> BF.lexer_interp_file "../helloworld.bf" [];
* \> BF.lex_interp_file "../helloworld.bf" [];

Haskell (GHC):

* $ ghc bf.hs
* $ ./bf < ../helloworld.bf
* $ rm bf.o bf.hi bf

C:

* $ gmake
* $ ./bf < ../helloworld.bf # or ./bf ../helloworld.bf
* $ gmake clean

### General Note ###

The given example was taken from: https://en.wikipedia.org/wiki/Brainfuck#Hello_World.21

### C Note ###

NOTE: This was an early attempt with a BF interpreter, I wouldn't actually put the C version in another program.

The interpreter is currently considered complete.  It will read the entire file (or stdin) into a buffer before executing.  All output will go into a buffer that is printed at the end.

The buffer method allows it to be planted in other programs. You just need to setup 4 buffers:

* one to be acted on
* one with the instructions
* one for input
* one for output,

then pass them to interpret_buffer.

