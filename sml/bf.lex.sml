structure BFLex=
   struct
    structure UserDeclarations =
      struct
(*#line 1.1 "/usr/home/chris/programming/bf-sml/bf.lex"*)datatype lexresult =
	INC
	| DEC
	| FORWARD
	| BACKWARD
	| OUTPUT
	| INPUT
	| LSTART
	| LEND
	| EOF
	| LOOP of lexresult list

val linenum = ref 1
val error = fn x => print (x ^ "\n")
val eof = fn () => EOF

(*#line 21.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
end (* end of user routines *)
exception LexError (* raised if illegal leaf action tried *)
structure Internal =
	struct

datatype yyfinstate = N of int
type statedata = {fin : yyfinstate list, trans: string}
(* transition & final state table *)
val tab = let
val s = [ 
 (0, 
"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\\000"
),
 (1, 
"\003\003\003\003\003\003\003\003\003\003\012\003\003\003\003\003\
\\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\
\\003\003\003\003\003\003\003\003\003\003\003\011\010\009\008\003\
\\003\003\003\003\003\003\003\003\003\003\003\003\007\003\006\003\
\\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\
\\003\003\003\003\003\003\003\003\003\003\003\005\003\004\003\003\
\\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\
\\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\
\\003"
),
(0, "")]
fun f x = x 
val s = map f (rev (tl (rev s))) 
exception LexHackingError 
fun look ((j,x)::r, i: int) = if i = j then x else look(r, i) 
  | look ([], i) = raise LexHackingError
fun g {fin=x, trans=i} = {fin=x, trans=look(s,i)} 
in Vector.fromList(map g 
[{fin = [], trans = 0},
{fin = [], trans = 1},
{fin = [], trans = 1},
{fin = [(N 19)], trans = 0},
{fin = [(N 17),(N 19)], trans = 0},
{fin = [(N 15),(N 19)], trans = 0},
{fin = [(N 7),(N 19)], trans = 0},
{fin = [(N 9),(N 19)], trans = 0},
{fin = [(N 11),(N 19)], trans = 0},
{fin = [(N 5),(N 19)], trans = 0},
{fin = [(N 13),(N 19)], trans = 0},
{fin = [(N 3),(N 19)], trans = 0},
{fin = [(N 1)], trans = 0}])
end
structure StartStates =
	struct
	datatype yystartstate = STARTSTATE of int

(* start state definitions *)

val INITIAL = STARTSTATE 1;

end
type result = UserDeclarations.lexresult
	exception LexerError (* raised if illegal leaf action tried *)
end

structure YYPosInt : INTEGER = Int
fun makeLexer yyinput =
let	val yygone0= YYPosInt.fromInt ~1
	val yyb = ref "\n" 		(* buffer *)
	val yybl = ref 1		(*buffer length *)
	val yybufpos = ref 1		(* location of next character to use *)
	val yygone = ref yygone0	(* position in file of beginning of buffer *)
	val yydone = ref false		(* eof found yet? *)
	val yybegin = ref 1		(*Current 'start state' for lexer *)

	val YYBEGIN = fn (Internal.StartStates.STARTSTATE x) =>
		 yybegin := x

fun lex () : Internal.result =
let fun continue() = lex() in
  let fun scan (s,AcceptingLeaves : Internal.yyfinstate list list,l,i0) =
	let fun action (i,nil) = raise LexError
	| action (i,nil::l) = action (i-1,l)
	| action (i,(node::acts)::l) =
		case node of
		    Internal.N yyk => 
			(let fun yymktext() = substring(!yyb,i0,i-i0)
			     val yypos = YYPosInt.+(YYPosInt.fromInt i0, !yygone)
			open UserDeclarations Internal.StartStates
 in (yybufpos := i; case yyk of 

			(* Application actions *)

  1 => ((*#line 23.8 "/usr/home/chris/programming/bf-sml/bf.lex"*)linenum := (!linenum) + 1; lex()(*#line 117.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 11 => ((*#line 28.9 "/usr/home/chris/programming/bf-sml/bf.lex"*)OUTPUT(*#line 119.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 13 => ((*#line 29.9 "/usr/home/chris/programming/bf-sml/bf.lex"*)INPUT(*#line 121.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 15 => ((*#line 30.9 "/usr/home/chris/programming/bf-sml/bf.lex"*)LSTART(*#line 123.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 17 => ((*#line 31.9 "/usr/home/chris/programming/bf-sml/bf.lex"*)LEND(*#line 125.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 19 => ((*#line 32.7 "/usr/home/chris/programming/bf-sml/bf.lex"*)lex()(*#line 127.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 3 => ((*#line 24.9 "/usr/home/chris/programming/bf-sml/bf.lex"*)INC(*#line 129.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 5 => ((*#line 25.9 "/usr/home/chris/programming/bf-sml/bf.lex"*)DEC(*#line 131.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 7 => ((*#line 26.9 "/usr/home/chris/programming/bf-sml/bf.lex"*)FORWARD(*#line 133.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| 9 => ((*#line 27.9 "/usr/home/chris/programming/bf-sml/bf.lex"*)BACKWARD(*#line 135.1 "/usr/home/chris/programming/bf-sml/bf.lex.sml"*)
)
| _ => raise Internal.LexerError

		) end )

	val {fin,trans} = Vector.sub(Internal.tab, s)
	val NewAcceptingLeaves = fin::AcceptingLeaves
	in if l = !yybl then
	     if trans = #trans(Vector.sub(Internal.tab,0))
	       then action(l,NewAcceptingLeaves
) else	    let val newchars= if !yydone then "" else yyinput 1024
	    in if (size newchars)=0
		  then (yydone := true;
		        if (l=i0) then UserDeclarations.eof ()
		                  else action(l,NewAcceptingLeaves))
		  else (if i0=l then yyb := newchars
		     else yyb := substring(!yyb,i0,l-i0)^newchars;
		     yygone := YYPosInt.+(!yygone, YYPosInt.fromInt i0);
		     yybl := size (!yyb);
		     scan (s,AcceptingLeaves,l-i0,0))
	    end
	  else let val NewChar = Char.ord(CharVector.sub(!yyb,l))
		val NewChar = if NewChar<128 then NewChar else 128
		val NewState = Char.ord(CharVector.sub(trans,NewChar))
		in if NewState=0 then action(l,NewAcceptingLeaves)
		else scan(NewState,NewAcceptingLeaves,l+1,i0)
	end
	end
(*
	val start= if substring(!yyb,!yybufpos-1,1)="\n"
then !yybegin+1 else !yybegin
*)
	in scan(!yybegin (* start *),nil,!yybufpos,!yybufpos)
    end
end
  in lex
  end
end
