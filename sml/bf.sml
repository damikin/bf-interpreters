use "bf.lex.sml";
use "lexer.sml";

structure BF =
struct
exception ArrayBounds
datatype lexresult =
	INC
	| DEC
	| FORWARD
	| BACKWARD
	| OUTPUT
	| INPUT
	| LSTART
	| LEND
	| EOF
	| LOOP of lexresult list

(* ML-Lex *)
fun lex_tokenize_file filename =
	let
		val file = TextIO.openIn filename
		fun get _ = TextIO.input file
		val lexer = BFLex.makeLexer get
		fun tokenize xs =
			let
				val t = lexer()
			in
				if t = BFLex.UserDeclarations.EOF
				then rev xs
				else tokenize (t::xs)
			end
		val tokens = tokenize []
		fun convert [] = []
		| convert (t::ts) = (case t of
			BFLex.UserDeclarations.INC => INC
			| BFLex.UserDeclarations.DEC => DEC
			| BFLex.UserDeclarations.FORWARD => FORWARD
			| BFLex.UserDeclarations.BACKWARD => BACKWARD
			| BFLex.UserDeclarations.OUTPUT => OUTPUT
			| BFLex.UserDeclarations.INPUT => INPUT
			| BFLex.UserDeclarations.LSTART => LSTART
			| BFLex.UserDeclarations.LEND => LEND
			| BFLex.UserDeclarations.EOF => EOF
			| BFLex.UserDeclarations.LOOP ts' => LOOP (convert ts')
			)::(convert ts)
	in
		(
			TextIO.closeIn file;
			convert tokens
		)
	end

(* hand written lexer *)
fun lexer_tokenize_file filename =
	let
		val file = TextIO.openIn filename
		val str = TextIO.input file
		fun convert [] = []
		| convert (t::ts) = (case t of
			Lexer.INC => INC
			| Lexer.DEC => DEC
			| Lexer.FORWARD => FORWARD
			| Lexer.BACKWARD => BACKWARD
			| Lexer.OUTPUT => OUTPUT
			| Lexer.INPUT => INPUT
			| Lexer.LSTART => LSTART
			| Lexer.LEND => LEND
			| Lexer.EOF => EOF
			| Lexer.LOOP ts' => LOOP (convert ts')
			)::(convert ts)
		val tokens = Lexer.run str
	in
		(
			TextIO.closeIn file;
			convert tokens
		)
	end

(* printing helpers *)
fun string_tokens ts =
	let
		fun f (t,acc) =
			acc ^ (case t of
				INC => "INC "
				| DEC => "DEC "
				| FORWARD => "FORWARD "
				| BACKWARD => "BACKWARD "
				| OUTPUT => "OUTPUT "
				| INPUT => "INPUT "
				| LSTART => "LSTART "
				| LEND => "LEND "
				| EOF => "EOF "
				| LOOP xs => "[" ^ (string_tokens xs) ^ "] ")
	in
		List.foldr f "" ts
	end

fun print_tokens ts = print (string_tokens ts)

(* parsing *)
fun parse_tokens ts =
	let
		fun p out [] = (out, [])
		| p out (t::ts) =
			case t of
				INC => p (INC::out) ts
				| DEC => p (DEC::out) ts
				| FORWARD => p (FORWARD::out) ts
				| BACKWARD => p (BACKWARD::out) ts
				| OUTPUT => p (OUTPUT::out) ts
				| INPUT => p (INPUT::out) ts
				| LSTART =>
					let val (out',ts') = p [] ts
					in p ((LOOP (rev out'))::out) ts'
					end
				| LEND => (out, ts)
				| EOF => (out, ts)
				| LOOP xs => p ((LOOP xs)::out) ts

		val (out,_) = p [] ts
	in
		rev out
	end

(* interpreter *)
fun interp ts input =
	let
		fun grow i mem =
			let
				val lm = length mem
				fun grow' 0 xs = xs
				| grow' i xs = grow' (i-1) (0::xs)
			in
				if i < lm
				then mem
				else mem @ (grow' (i - lm + 1) [])
			end

		fun inc i mem v =
			let
				fun inc' 0 (m::mem) v = (m+v)::mem
				| inc' i (m::mem) v =
					if i < 0
					then raise ArrayBounds
					else m::(inc' (i-1) mem v)
			in
				inc' i (grow i mem) v
			end

		fun get i mem =
			let
				fun get' 0 (m::_) = m
				| get' i (_::mem) =
						if i < 0
						then raise ArrayBounds
						else get' (i-1) mem
			in
				get' i (grow i mem)
			end

		fun change i mem v =
			let
				fun change' 0 (_::mem) v = v::mem
				| change' i (_::mem) v =
						if i < 0
						then raise ArrayBounds
						else change' (i-1) mem v
			in
				change' i (grow i mem) v
			end

		fun p [] input i mem = (input,i,mem)
		| p (t::ts) input i mem =
			case t of
				INC => p ts input i (inc i mem 1)
				| DEC => p ts input i (inc i mem ~1)
				| FORWARD => p ts input (i+1) mem
				| BACKWARD => p ts input (i-1) mem
				| OUTPUT =>
					let
						val c = chr (get i mem)
						val s = String.str c
					in
						(print s; p ts input i mem)
					end
				| INPUT =>
					let val (v::input') = input
					in p ts input' i (change i mem v)
					end
				| LOOP ts' =>
					if 0 = (get i mem)
					then p ts input i mem
					else
						let val (input',i',mem') = p ts' input i mem
						in p (t::ts) input' i' mem'
						end

		val (input,i,mem) = p ts input 0 []
	in
		mem
	end

(* run files *)
fun lexer_interp_file filename input =
	let
		val ts = lexer_tokenize_file filename
		val ts' = parse_tokens ts
	in
		interp ts' input
	end

fun lex_interp_file filename input =
	let
		val ts = lex_tokenize_file filename
		val ts' = parse_tokens ts
	in
		interp ts' input
	end

end

