structure Lexer =
struct
datatype lexresult =
	INC
	| DEC
	| FORWARD
	| BACKWARD
	| OUTPUT
	| INPUT
	| LSTART
	| LEND
	| EOF
	| LOOP of lexresult list

datatype Result =
	Success of lexresult option * string
	| Failure of string

datatype Lexer = Lexer of (string -> Result)

(* easy to run and get answers *)
fun lrun (Lexer l) str = l str

(* alternation *)
infix 6 <|>
fun l1 <|> l2 =
	let fun f str =
		let val r1 = lrun l1 str
		in case r1 of
			Success r => r1
			| Failure err => lrun l2 str
		end
	in Lexer f
	end

(* a parser for characters *)
fun lchar c tok =
	let fun f str =
		if String.size str = 0
		then
			if String.size c = 0
			then Success (NONE, str)
			else Failure str
		else
			let
				val c' = String.extract(str, 0, SOME 1)
			in
				if c = c'
				then  Success (SOME tok, String.extract(str, 1, NONE))
				else Failure str
			end
	in Lexer f
	end

val toks =
	(lchar "+" INC) <|>
	(lchar "-" DEC) <|>
	(lchar ">" FORWARD) <|>
	(lchar "<" BACKWARD) <|>
	(lchar "." OUTPUT) <|>
	(lchar "," INPUT) <|>
	(lchar "[" LSTART) <|>
	(lchar "]" LEND)

fun run str =
	let fun f ts str =
		if String.size str = 0
		then EOF::ts
		else
			let
				val rest = String.extract(str, 1, NONE)
				val t = lrun toks str
			in case t of
				Failure _ => f ts rest
				| Success (t', rest') =>
					(case t' of
						NONE => f ts rest'
						| SOME t'' => f (t''::ts) rest')
			end
	in rev (f [] str)
	end

end

