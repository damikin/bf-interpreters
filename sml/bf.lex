datatype lexresult =
	INC
	| DEC
	| FORWARD
	| BACKWARD
	| OUTPUT
	| INPUT
	| LSTART
	| LEND
	| EOF
	| LOOP of lexresult list

val linenum = ref 1
val error = fn x => print (x ^ "\n")
val eof = fn () => EOF

%%

%structure BFLex

%%

\n => (linenum := (!linenum) + 1; lex());
"+" => (INC);
"-" => (DEC);
">" => (FORWARD);
"<" => (BACKWARD);
"." => (OUTPUT);
"," => (INPUT);
"[" => (LSTART);
"]" => (LEND);
. => (lex());

