#include <stdio.h>
#include <stdlib.h>
#include "buffer.h"
#include "interpreter.h"

enum {
	BUF_LEN = 30000
};

struct buffer_s *bf_stdin(void);
struct buffer_s *bf_filein(char *);
void bf_error(char *);
void bf_print(struct buffer_s *);

int
main(int argc, char **argv) {
	struct buffer_s *bf = NULL;
	struct buffer_s *inst = NULL;
	struct buffer_s *in = NULL;
	struct buffer_s *out = NULL;

	/* the 4 buffers (bf, instructions, input, output) */
	if(argc > 1)
		inst = bf_filein(argv[1]);
	else
		inst = bf_stdin();

	bf = buffer_new(BUF_LEN);
	in = buffer_new(2);
	out = buffer_new(2);

	if(!bf)
		bf_error("bf buffer failed");
	else if(!inst)
		bf_error("instruction buffer failed");
	else if(!in)
		bf_error("input buffer failed");
	else if(!out)
		bf_error("output buffer failed");
	else {
		interpret_buffer(bf, inst, in, out);
		bf_print(out);
	}

	buffer_delete(bf);
	buffer_delete(inst);
	buffer_delete(in);
	buffer_delete(out);

	return 0;
}

/*
 * helper functions
 */
struct buffer_s *
bf_stdin() {
	struct buffer_s *buf;
	buf = buffer_new(2);
	if(!buf)
		return NULL;

	for(int c = fgetc(stdin); c != EOF; c = fgetc(stdin))
		buffer_putc(buf, c);

	return buf;
}

struct buffer_s *
bf_filein(char *file) {
	FILE *fs;
	fs = fopen(file, "r");
	if(!fs)
		return NULL;

	struct buffer_s *buf;
	buf = buffer_new(2);
	if(!buf)
		return NULL;

	for(int c = fgetc(fs); c != EOF; c = fgetc(fs))
		buffer_putc(buf, c);

	return buf;
}

void
bf_error(char *str) {
	fprintf(stderr, "%s\n", str);
	exit(1);
}

void
bf_print(struct buffer_s *out) {
	for(int c = buffer_getc(out); c != EOF; c = buffer_getc(out))
		fputc(c, stdout);
	fputc('\n', stdout);
}

