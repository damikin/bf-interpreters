#include <stdio.h>
#include "buffer.h"
#include "interpreter.h"

int
interpret(bp bf, char c) {
	int ret = INTERPRET_OK;

	switch(c) {
	case '+': buffer_inc(bf); break;
	case '-': buffer_dec(bf); break;
	case '>': buffer_next(bf); break;
	case '<': buffer_prev(bf); break;
	case '.': ret = INTERPRET_OUTPUT; break;
	case ',': ret = INTERPRET_INPUT; break;
	case '[': ret = INTERPRET_LOOP_START; break;
	case ']': ret = INTERPRET_LOOP_END; break;
	default: break;
	}

	return ret;
}

int
interpret_buffer(bp bf, bp inst, bp in, bp out) {
	struct buffer_s *loop = NULL;

	for(int c = buffer_getc(inst); c != EOF; c = buffer_getc(inst)) {
		switch(interpret(bf, c)) {
		case INTERPRET_OUTPUT:
			buffer_putc(out, buffer_value(bf));
			break;

		case INTERPRET_INPUT:
			c = buffer_getc(in);
			if(c == EOF)
				c = 0;
			buffer_set(bf, c);
			break;

		case INTERPRET_LOOP_START:
			loop = interpret_loop(inst);
			while(buffer_value(bf)) {
				if(interpret_buffer(bf, loop, in, out) < 0) {
					fprintf(stderr, "Mismatched loop!\n");
					break;
				}
				buffer_reset(loop);
			}
			buffer_delete(loop);
			loop = NULL;
			break;

		case INTERPRET_LOOP_END:
			/* mismatched loop! [ ] */
			return -1;

		default:
			/* do nothing */
			break;
		}
	}

	return 0;
}

struct buffer_s *
interpret_loop(struct buffer_s *inst) {
	struct buffer_s *loop;
	loop = buffer_new(2);
	if(!loop)
		return NULL;

	int cnt = 0;

	for(int c = buffer_getc(inst); c != EOF; c = buffer_getc(inst)) {
		if(c == ']' && cnt <= 0)
			break;
		
		if(c == ']')
			cnt--;

		if(c == '[')
			cnt++;

		buffer_putc(loop, c);
	}

	return loop;
}

