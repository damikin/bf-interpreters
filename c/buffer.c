#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include "buffer.h"

/*
 * helper functions
 */
int buffer_check(struct buffer_s *);

/*
 * structs
 */
struct buffer_s {
	char *buffer;
	char *end;
	char *p;
	size_t size;
};

/*
 * exported functions
 */
struct buffer_s *
buffer_new(size_t size) {
	struct buffer_s *buf;
	buf = calloc(1, sizeof(*buf));
	if(!buf)
		return NULL;

	buf->buffer = calloc(size, sizeof(*buf->buffer));
	if(!buf->buffer) {
		buffer_delete(buf);
		return NULL;
	}

	buf->end = buf->buffer;
	buf->p = buf->buffer;
	buf->size = size;

	return buf;
}

void
buffer_delete(struct buffer_s *buf) {
	free(buf->buffer);
	free(buf);
}

int
buffer_getc(struct buffer_s *buf) {
	if(buf->p >= buf->end)
		return EOF;

	int ret = *buf->p;
	buffer_next(buf);
	return ret;
}

int
buffer_putc(struct buffer_s *buf, char c) {
	/* 0 based indexing, > is an off by 1 error */
	if(buf->end >= buf->buffer + buf->size)
		if(buffer_resize(buf) < 0)
			return -1;

	*buf->end = c;
	buf->end++;
	return 0;
}

void
buffer_reset(struct buffer_s *buf) {
	buf->p = buf->buffer;
}

int
buffer_resize(struct buffer_s *buf) {
	size_t size;
	if(buf->size == SIZE_MAX)
		return -1;

	if(SIZE_MAX / 2 < buf->size)
		size = SIZE_MAX;
	else
		size = buf->size * 2;

	ptrdiff_t diffp = buf->p - buf->buffer;
	ptrdiff_t diffend = buf->end - buf->buffer;

	char *ret = realloc(buf->buffer, size);
	if(!ret)
		return -1;

	buf->buffer = ret;
	buf->p = buf->buffer + diffp;
	buf->end = buf->buffer + diffend;
	buf->size = size;

	return 0;
}

void
buffer_next(struct buffer_s *buf) {
	buf->p++;
}

void
buffer_prev(struct buffer_s *buf) {
	buf->p--;
}

int
buffer_inc(struct buffer_s *buf) {
	if(buffer_check(buf) < 0)
		return -1;

	(*buf->p)++;
	return 0;
}

int
buffer_dec(struct buffer_s *buf) {
	if(buffer_check(buf) < 0)
		return -1;

	(*buf->p)--;
	return 0;
}

int
buffer_set(struct buffer_s *buf, char c) {
	if(buffer_check(buf) < 0)
		return -1;

	*buf->p = c;
	return 0;
}

int
buffer_value(struct buffer_s *buf) {
	if(buffer_check(buf) < 0)
		return EOF;
	return *buf->p;
}

int
buffer_check(struct buffer_s *buf) {
	if(buf->p < buf->buffer)
		return -1;

	if(buf->p >= buf->buffer + buf->size)
		return -2;

	return 0;
}

