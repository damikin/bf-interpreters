#ifndef INTERPRETER_H_
#define INTERPRETER_H_

enum {
	  INTERPRET_OK
	, INTERPRET_INPUT
	, INTERPRET_OUTPUT
	, INTERPRET_LOOP_START
	, INTERPRET_LOOP_END
};

typedef struct buffer_s *bp;

int interpret(bp, char);
int interpret_buffer(bp, bp, bp, bp);

struct buffer_s *interpret_loop(struct buffer_s *);

#endif

