#ifndef BUFFER_H_
#define BUFFER_H_

struct buffer_s;

struct buffer_s *buffer_new(size_t);
void buffer_delete(struct buffer_s *);

int buffer_getc(struct buffer_s *);
int buffer_putc(struct buffer_s *, char);

void buffer_reset(struct buffer_s *);
int buffer_resize(struct buffer_s *);

void buffer_next(struct buffer_s *);
void buffer_prev(struct buffer_s *);

int buffer_inc(struct buffer_s *);
int buffer_dec(struct buffer_s *);

int buffer_set(struct buffer_s *, char);
int buffer_value(struct buffer_s *);

#endif

